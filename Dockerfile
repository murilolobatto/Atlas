FROM php:7.2-apache

RUN rm /etc/apache2/sites-enabled/*

RUN a2enmod rewrite

COPY docker/apache2/sites-enabled/001-symfony.conf /etc/apache2/sites-enabled/

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl https://phar.phpunit.de/phpunit-6.5.phar -L -o phpunit-6.5.phar && chmod +x phpunit-6.5.phar && mv phpunit-6.5.phar /usr/local/bin/phpunit

RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update && apt-get install -y git

RUN apt-get install -y zlib1g-dev && docker-php-ext-install zip

RUN docker-php-ext-install mbstring