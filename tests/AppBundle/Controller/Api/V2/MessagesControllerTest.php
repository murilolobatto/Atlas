<?php

namespace Tests\AppBundle\Controller\Api\V2;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MessagesControllerTest extends WebTestCase
{
    public function testApiRequiresAuthentication()
    {
        $anonymousClient = static::createClient();

        $anonymousClient->request('GET', '/api/v2/messages');

        $this->assertEquals(401, $anonymousClient->getResponse()->getStatusCode());

        $authenticatedClient = $this->getAuthenticatedClient();

        $authenticatedClient->request('GET', '/api/v2/messages');

        $this->assertEquals(200, $authenticatedClient->getResponse()->getStatusCode());
    }

    public function testGetMessages()
    {
        $client = $this->getAuthenticatedClient();

        $client->request('GET', '/api/v2/messages');

        $response = $client->getResponse();

        $this->assertEquals('application/json', $response->headers->get('content-type'));

        $envelope = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('data', $envelope);
        $this->assertTrue(is_array($envelope['data']));
    }

    public function testGetMessage()
    {
        $client = $this->getAuthenticatedClient();

        $client->request('GET', '/api/v2/messages/1');

        $response = $client->getResponse();

        $this->assertEquals('application/json', $response->headers->get('content-type'));

        $message = json_decode($response->getContent(), true);

        $expectedResponse = [ 'data' => [
            'id' => '1',
            'content' => 'This is a message'
        ]];

        $this->assertArraySubset($message, $expectedResponse);
    }

    public function testPostMessage()
    {
        $client = $this->getAuthenticatedClient();

        $client->request('POST', '/api/v2/messages', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'content' => 'Message created through API',
        ]));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testPutMessage()
    {
        $client = $this->getAuthenticatedClient();

        $client->request('POST', '/api/v2/messages', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'content' => 'Message with typo',
        ]));

        $message = json_decode($client->getResponse()->getContent(), true);

        $client->request('PUT', '/api/v2/messages/'.$message['id'], [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'content' => 'Message without typo',
        ]));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDeleteMessage()
    {
        $client = $this->getAuthenticatedClient();

        $client->request('POST', '/api/v2/messages', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'content' => 'Random message',
        ]));

        $message = json_decode($client->getResponse()->getContent(), true);

        $client->request('DELETE', '/api/v2/messages/'.$message['id']);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    public function testOauthAuthentication()
    {
        $client = static::createClient();

        $OauthClientManager = $client->getContainer()->get('fos_oauth_server.client_manager.default');
        $OauthClient = $OauthClientManager->createClient();
        $OauthClient->setAllowedGrantTypes(array('password'));
        $OauthClientManager->updateClient($OauthClient);

        $client->request('GET', '/oauth/v2/token?client_id='.$OauthClient->getPublicId().'&client_secret='.$OauthClient->getSecret().'&grant_type=password&username=foo&password=bar');

        $accessToken = json_decode($client->getResponse()->getContent(), true);

        $client->request('GET', '/api/v2/messages', [], [], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . $accessToken['access_token'],
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    private function getAuthenticatedClient()
    {
        return static::createClient([], [
            'PHP_AUTH_USER' => 'foo',
            'PHP_AUTH_PW' => 'bar'
        ]);
    }
}
