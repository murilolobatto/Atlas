<?php

$bootstrapStart = microtime(true);

passthru(sprintf(
    'php %s/../bin/console doctrine:database:drop --env=%s --force',
    __DIR__,
    'test'
));

passthru(sprintf(
    'php %s/../bin/console doctrine:database:create --env=%s',
    __DIR__,
    'test'
));

passthru(sprintf(
    'php %s/../bin/console doctrine:migrations:migrate --no-interaction --env=%s ',
    __DIR__,
    'test'
));

passthru(sprintf(
    'php %s/../bin/console doctrine:fixtures:load --env=%s --no-interaction',
    __DIR__,
    'test'
));

$bootstrapEnd = microtime(true);

passthru(sprintf(
    'echo "The bootstrap process took %d seconds"',
    $bootstrapEnd - $bootstrapStart
));

require __DIR__.'/../vendor/autoload.php';

