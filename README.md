Atlas
=====

Another Twitter Like Aplication Software :)

## How to setup you development environment:

```bash
# Clear temporary directories
sudo rm -rf var/cache/* var/logs/* var/sessions/*

# Build and start docker containers
docker-compose up -d --build

# Install project dependencies
docker exec -it atlas_webserver composer install

# Create database
docker exec -it atlas_webserver php bin/console doctrine:database:create

# Execute migrations
docker exec -it atlas_webserver php bin/console doctrine:migr:migrate --no-interaction

# Gives permissive permissions to temp directories shared with the container, warning, do this in development only.
sudo chmod -R 777 var/cache var/logs var/sessions
```

## How to run automated tests:

```bash
# Run tests
docker exec -it atlas_webserver phpunit

# Reset permissive permissions
sudo chmod -R 777 var/cache var/logs var/sessions
```
