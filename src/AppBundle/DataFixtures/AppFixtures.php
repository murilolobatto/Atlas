<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('foo');
        $user->setPlainPassword('bar');
        $user->setEmail('foo@bar.baz');
        $user->setEnabled(true);

        $manager->persist($user);
        $manager->flush();

        $message = new Message();
        $message->setUser($user);
        $message->setContent('This is a message');

        $manager->persist($message);
        $manager->flush();
    }
}
