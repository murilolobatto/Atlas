<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_USER')")
 */
class MessageController extends Controller
{
    /**
     * @Route("/message", name="message_index", methods={"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setUser($this->getUser());
            $em->persist($message);
            $em->flush();

            $this->addFlash('success', 'Message successfully created!');

            return $this->redirect($this->generateUrl('message_index'));
        }

        $messages = $em->getRepository("AppBundle:Message")->findBy([], ['createdAt' => 'desc']);

        return $this->render('message/index.html.twig', [
            'form' => $form->createView(),
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/message/{id}", methods={"DELETE"}, name="message_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository("AppBundle:Message")->find($id);

        if(!$message) {
            return $this->createNotFoundException();
        }

        $this->denyAccessUnlessGranted('delete', $message);

        $em->remove($message);
        $em->flush();

        $this->addFlash('success', 'Message successfully removed');

        return $this->redirect($this->generateUrl('message_index'));
    }
}
