<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_USER')")
 */
class MessagesController extends FOSRestController  implements ClassResourceInterface
{
    public function cgetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository('AppBundle:Message')->findBy([], ['createdAt' => 'DESC']);
        $view = $this->view($messages, 200);

        return $this->handleView($view);
    }

    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('AppBundle:Message')->find($id);
        $view = $this->view($message, 200);

        return $this->handleView($view);
    }

    public function postAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $message = new Message();

        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'POST',
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setUser($this->getUser());
            $em->persist($message);
            $em->flush();

            $view = $this->view($message, 201);
            return $this->handleView($view);
        }

        $view = $this->view($form);
        return $this->handleView($view);
    }

    public function putAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository("AppBundle:Message")->find($id);

        $this->denyAccessUnlessGranted('edit', $message);

        $form = $this->createForm(MessageType::class, $message, [
            'method' => 'POST',
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setUser($this->getUser());
            $em->persist($message);
            $em->flush();

            $view = $this->view($message, 200);
            return $this->handleView($view);
        }

        $view = $this->view($form);
        return $this->handleView($view);
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $message = $em->getRepository("AppBundle:Message")->find($id);

        $this->denyAccessUnlessGranted('delete', $message);

        $em->remove($message);
        $em->flush();

        $view = $this->view(null, 204);

        return $this->handleView($view);
    }
}
